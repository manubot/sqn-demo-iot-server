
import argparse
import sys
from http_client import HTTPClient

args = sys.argv

parser = argparse.ArgumentParser(
    description='Test client for SQN Demot IoT Server')
parser.add_argument('-P',
                    choices=['HTTP'], default='HTTP', dest="protocol",
                    help="Protocol to use for sending payload. Default is HTTP")
parser.add_argument('-s', '--server_ip', default="localhost",
                    help="server ip or URL. Defaul is localhost")
parser.add_argument('-p', '--server_port', default=8080, type=int,
                    help="server port. Default it 8080")
parser.add_argument('-f', '--payload_file', type=argparse.FileType('r'),
                    default="sample_payload.json",
                    help="Name of the file containing the payload to send to the server, \
                    if none is provided uses sample_payload.json")
parser.add_argument('-v', action='store_true',
                    dest="verbose",
                    help="To activate verbose output")
parser.add_argument('-l', '--loop', action='store_true', default=True,
                    dest="loop",
                    help="To run in loop mode. True by default")
parser.add_argument('-d', '--delay', default=60, type=int,
                    dest="delay",
                    help="delay between each payload send. 60s by default")
parser.add_argument('-r', '--reg', action='store_true', default=True,
                    dest="register_needed",
                    help="if device requires to be registered. True by default")
parser.add_argument('--auth', action='store_true',
                    dest="auth",
                    help="Enables JWT Authentication. Can be used with --auth_key_file'")
parser.add_argument('--auth_key_file', default="rsa_private.pem", type=argparse.FileType('r'),
                    help="When JWT Authentication is enabled, allows specifying the file containing \
                    private key to be used. By default uses rsa_private.pem")

args = parser.parse_args(args[1:])

if args.protocol == "HTTP" or "HTTPS":
    client = HTTPClient(args)
