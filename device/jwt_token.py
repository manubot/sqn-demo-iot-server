
import jwt
import datetime


def create_jwt(private_key, algorithm='RS256', project="sqn-iot"):
    """Creates a JWT (https://jwt.io).
        Args:
         private_key: The private RSA256 key
         algorithm: The encryption algorithm to use. Only 'RS256' supported now
         project: The project this device belongs to
        Returns:
            A JWT generated from the given project_id and private key, which
            expires in 20 minutes. After 20 minutes, your client will be
            disconnected, and a new JWT will have to be generated.
        Raises:
            ValueError: If the private_key_file does not contain a known key.
        """

    token = {
        # The time that the token was issued at
        'iat': datetime.datetime.utcnow(),
        # The time the token expires.
        'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=60),
        # The audience field should be set to match the server project name
        # In this demo server the default value is sqn-iot
        'aud': project
    }

    return jwt.encode(token, private_key, algorithm=algorithm)


# The JWT header consists of two fields that indicate the signing
# algorithm and the type of token. Both fields are mandatory,
# and each field has only one value. The following signing algorithm is  supported:

# JWT RS256 (RSASSA-PKCS1-v1_5 using SHA-256 RFC 7518 sec 3.3).
# This is expressed as RS256 in the alg field in the JWT header.
