Script to simulate iot device in python3

# Run in local machine

- `pip install --requirement requirements.txt`
- run `python main.py --help` to check usage

# Run in docker container

- `docker build . -t device`
- To run the container need to specify the run command: `docker run -it device python main.py -s 35.246.63.217 -p 8080 -r`
