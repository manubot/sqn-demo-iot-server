import requests
import json
import time
import jwt_token as jwt
from datetime import datetime
import random


def get_data_payload(payload_file):
    payload = payload_file.read()
    return payload


def get_private_key(key_file):
    return key_file.read()


class HTTPClient():
    def __init__(self, args):
        self.args = args
        self.api_url = "{}://{}:{}/api".format(
            args.protocol.lower(), args.server_ip, args.server_port)
        payload_file = get_data_payload(self.args.payload_file)
        payload_array = json.loads(payload_file)

        if args.auth and args.auth_key_file:
            self.key = get_private_key(args.auth_key_file)

        if self.args.register_needed:
            self.send_payload(payload_array[0], force_post=True)
            payload_array = payload_array[1:]
            time.sleep(args.delay)

        for payload in payload_array:
            self.send_payload(payload)
            time.sleep(args.delay)

        while(args.loop):
            payload_array = json.loads(payload_file)
            for payload in payload_array:
                self.send_payload(payload)
                time.sleep(args.delay)

    def add_sensor_data(self, payload):
        payload['sensors'] = [
            {
                'timestamp': datetime.utcnow().isoformat(),
                'sensor_type': 'temperature',
                'value': random.random() * 10 + 40,
                'units': 'degrees celsius'
            },
            {
                'timestamp': datetime.utcnow().isoformat(),
                'sensor_type': 'light',
                'value': random.random() * 10 + 100,
                'units': 'lumens'
            },
            {
                'timestamp': datetime.utcnow().isoformat(),
                'sensor_type': 'humidity',
                'value': random.random() * 10 + 80,
                'units': '%'
            },
            {
                'timestamp': datetime.utcnow().isoformat(),
                'sensor_type': 'pressure',
                'value': random.random() * 100 + 1500,
                'units': 'atmospheres'
            }
        ]

    def reduce_positions_update(self, payload):
        if random.random() < 0.5:
            payload.pop('cellid')

    def add_capabilities(self, payload):
        payload['capabilities'] = {
            'lpm': True,
            'leds': 1,
            'period': True
        }

    def add_events(self, payload):
        events = []
        toss = random.random()
        if toss > 0.5:
            events.append({
                'timestamp': datetime.utcnow().isoformat(),
                'event_type': 'button_press'})
        if toss > 0.7 and toss < 0.85:
            events.append({
                'timestamp': datetime.utcnow().isoformat(),
                'event_type': 'switch_on'})
        if toss >= 0.85:
            events.append({
                'timestamp': datetime.utcnow().isoformat(),
                'event_type': 'switch_off'})
        payload['events'] = events

    def send_payload(self, payload, force_post=False):
        if force_post:
            post_url = self.api_url + "/device"
            self.add_capabilities(payload)
            print("Sending POST to {}".format(post_url))
            print(payload)
            self.http_request("POST", post_url, json.dumps(payload), 201)
        else:
            self.add_sensor_data(payload)
            self.reduce_positions_update(payload)
            self.add_events(payload)
            patch_url = self.api_url + "/device/{}".format(payload["imei"])
            print("Sending PATCH to {}".format(patch_url))
            print(payload)
            self.http_request("PATCH", patch_url, json.dumps(payload), 200)

    def parse_response(self, response):
        if "period" in response:
            period = response['period']
            if period:
                self.args.delay = period

    def http_request(self, method, url, data, expected_code):

        headers = {}
        if self.args.auth:
            token = jwt.create_jwt(self.key)
            headers['Authorization'] = "Bearer " + token.decode()

        start = time.clock()

        if method == "POST":
            r = requests.post(url, data=data, headers=headers,
                              allow_redirects=False, timeout=5)
        elif method == "PATCH":
            r = requests.post(url, data=data, headers=headers,
                              allow_redirects=False, timeout=5)

        request_time = time.clock() - start

        if r.status_code == expected_code:
            print("Payload succesfully sent over {} in {}ms".format(
                self.args.protocol, request_time * 1000))
            print("Response: {}".format(r.json()))
            self.parse_response(r.json())
            print("\n")
        else:
            raise Exception("Failed to send payload. \
                Error code: {}. Reason: {}. Details: {}".format(
                r.status_code, r.reason, r.content.decode()))
