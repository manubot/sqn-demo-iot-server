from datetime import datetime
from pydantic import BaseModel, Schema
from enum import Enum
from typing import List


class cellIDObs(BaseModel):
    mcc: int = None
    mnc: int = None
    lac: int = None
    cid: int = None
    psc: int = None
    age: int = None
    sstrength: int = None
    tadvance: int = None


class GeoPt(BaseModel):
    latitude: str
    longitude: str
    accuracy: int
    timestamp: datetime

class SensorType(str, Enum):
    temperature = 'temperature'
    light = 'light'
    pressure = 'pressure'
    humidity = 'humidity'


class SensorData (BaseModel):
    timestamp: datetime
    value: int
    sensor_type: SensorType
    units: str


class EventType(str, Enum):
    button_press = 'button_press'
    switch_on = 'switch_on'
    switch_off = 'switch_off'


class Event (BaseModel):
    timestamp: datetime
    event_type: EventType


class LowPowerMode(str, Enum):
    eDRX = 'eDRX'
    PSM = 'PSM'
    IDLE = 'IDLE'


class Config (BaseModel):
    lpm: LowPowerMode = LowPowerMode.PSM
    leds: List[bool] = None
    period: int = 10


class Capabilities(BaseModel):
    lpm: bool = Schema(False, description="Ability to switch low power modes")
    leds: int = Schema(0, description="number of leds")
    period: bool = Schema(False, description="Ability to control report periodicity")


class DeviceAPI(BaseModel):

    imei: str = Schema(..., max_length=17, min_length=15)
    hardware: str = None
    chipset: str = None
    created_at: datetime = Schema(None, readOnly=True)
    last_update: datetime = Schema(None, readOnly=True)
    cellid: cellIDObs = None
    last_apn: str = None
    sensors: List[SensorData] = None
    events: List[Event] = None
    capabilities: Capabilities = Capabilities()

    def __init__(self, *initial_data, **kwargs):
        BaseModel.__init__(self, *initial_data, **kwargs)
        self.created_at = datetime.utcnow()
        self.last_update = datetime.utcnow()

    def update(self, new_device):
        if new_device.hardware:
            self.hardware = new_device.hardware
        if new_device.chipset:
            self.chipset = new_device.chipset
        if new_device.cellid:
            self.cellid = new_device.cellid
        if new_device.last_apn:
            self.last_apn = new_device.last_apn
        if new_device.sensors:
            self.sensors = new_device.sensors
        if new_device.events:
            self.events = new_device.events
        self.last_update = datetime.utcnow()


class DeviceUI(DeviceAPI):
    position: GeoPt = None
    config: Config = Config()
    synced: bool = False 

