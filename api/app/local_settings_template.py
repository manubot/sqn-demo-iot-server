# Edit and rename to local_settings.py

# This is the key for using Google Geolocation API
GOOGLE_GEOLOC_API_KEY = "key"

# This is the IP or URL of the UI server. It is required for CORS
ORIGINS_PRODUCTION_SERVER = "http://<url>"

# This is the public key for JWT signature verification
RSA_PUBLIC_KEY = b'-----BEGIN PUBLIC KEY-----\
key \
-----END PUBLIC KEY-----'
