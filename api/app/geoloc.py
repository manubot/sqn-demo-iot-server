import requests
import json
from requests.exceptions import HTTPError, Timeout
from app.local_settings import GOOGLE_GEOLOC_API_KEY
from app.models import GeoPt
from app.cache import cache_provider
import logging
from datetime import datetime
logger = logging.getLogger(__name__)


class LocateException(Exception):
    pass


def geolocate_device(device_id):
    cache = cache_provider()
    try:
        device = cache.get(device_id)
        if device.cellid:
            lat, lng, acc = _geolocate_google([device.cellid, ])
            device.position = GeoPt(latitude=lat,
                                    longitude=lng,
                                    accuracy=acc,
                                    timestamp=datetime.utcnow())
        cache.set(device.imei, device)
        logger.info(
            "Succesfully updated position of device with id {}"
            .format(device.imei))
    except LocateException as e:
        device.position = None
        cache.set(device.imei, device)
        logger.warning(format(e))


def _geolocate_google(cellid_obs, wifi_obs=False):

    _GOOGLE_GEOLOCATION_BASE_URL = "https://www.googleapis.com"

    wifi_aps = None
    cell_towers = None
    if wifi_obs:
        wifi_aps = []
        for obs in wifi_obs:
            new_ap = {}
            if obs.macaddr:
                new_ap["macAddress"] = obs.macaddr
            if obs.rssi:
                new_ap["signalStrength"] = obs.rssi
            if obs.age:
                new_ap["age"] = obs.age
            if obs.channel:
                new_ap["channel"] = obs.channel
            if obs.snr:
                new_ap["signalToNoiseRatio"] = obs.snr
            wifi_aps.append(new_ap)

    if cellid_obs:
        for obs in cellid_obs:
            cell_towers = []
            new_tower = {}
            if obs.cid:
                new_tower["cellId"] = obs.cid
            if obs.lac:
                new_tower["locationAreaCode"] = obs.lac
            if obs.mcc:
                new_tower["mobileCountryCode"] = obs.mcc
            if obs.mnc:
                new_tower["mobileNetworkCode"] = obs.mnc
            if obs.age:
                new_tower["age"] = obs.age
            if obs.sstrength:
                new_tower["signalStrength"] = obs.sstrength
            if obs.tadvance:
                new_tower["timingAdvance"] = obs.tadvance
            cell_towers.append(new_tower)

    params = {}

    params["radioType"] = "lte"
    params["carrier"] = None
    params["considerIp"] = False

    if cell_towers is not None:
        params["cellTowers"] = cell_towers
        if "mobileCountryCode" in cell_towers[0]:
            params["homeMobileCountryCode"] = cell_towers[0]["mobileCountryCode"]
        if "mobileNetworkCode" in cell_towers[0]:
            params["homeMobileNetworkCode"] = cell_towers[0]["mobileNetworkCode"]
    else:
        params["homeMobileCountryCode"] = None
        params["homeMobileNetworkCode"] = None
    if wifi_aps is not None:
        params["wifiAccessPoints"] = wifi_aps

    url = _GOOGLE_GEOLOCATION_BASE_URL + \
        "/geolocation/v1/geolocate?key=" + GOOGLE_GEOLOC_API_KEY
    data = json.dumps(params)
    # print(data)
    headers = {"Content-Type": "application/json"}
    try:
        response = requests.post(url=url,
                                 headers=headers,
                                 data=data,
                                 timeout=5)
    except (HTTPError, Timeout) as e:
        raise LocateException("locate exception" + e.read())

    result = response.json()

    if response.status_code != 200 or "location" not in result:
        raise LocateException("LocateException : {error: %s, message: %s}" % (
            result["error"]["code"], result["error"]["message"]))

    return result['location']['lat'], result['location']['lng'], result["accuracy"]
