from fastapi import APIRouter, Header, BackgroundTasks, Depends
from app.models import *
from app.geoloc import geolocate_device
from starlette.status import HTTP_201_CREATED
from app.cache import cache_provider
from app.auth import jwt_decode

router = APIRouter()


@router.post('/api/device',
             response_model=Config,
             status_code=HTTP_201_CREATED)
def post_device(
        device: DeviceAPI,
        background_tasks: BackgroundTasks,
        authorization: str = Header(None),
        cache=Depends(cache_provider)):

    jwt_decode(authorization)
    db_device = DeviceUI(**device.dict())
    db_device.synced = True
    cache.set(db_device.imei, db_device)
    background_tasks.add_task(geolocate_device, db_device.imei)
    return db_device.config


@router.post('/api/device/{device_id}',
             response_model=Config)
def patch_device(
        device_id: str,
        new_device: DeviceAPI,
        background_tasks: BackgroundTasks,
        authorization: str = Header(None),
        cache=Depends(cache_provider)):

    jwt_decode(authorization)
    new_device = DeviceUI(**new_device.dict())
    if new_device.imei != device_id:
        raise HTTPException(
            status_code=403,
            detail="api parameter does not match json contents")
    device = cache.get(device_id)

    update_location = False
    if new_device.cellid:
        if new_device.cellid.cid != device.cellid.cid \
                or new_device.cellid.mcc != device.cellid.mcc \
                or new_device.cellid.mnc != device.cellid.mnc \
                or new_device.cellid.lac != device.cellid.lac:
            update_location = True

    device.update(new_device)
    device.synced = True
    cache.set(device.imei, device)

    if update_location:
        background_tasks.add_task(geolocate_device, device.imei)

    return device.config
