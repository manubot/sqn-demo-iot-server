from fastapi import APIRouter, Depends
from app.models import *
from app.cache import cache_provider

router = APIRouter()


@router.get('/api/device/{device_id}',
            response_model=DeviceUI)
def get_device(
        device_id: str,
        cache=Depends(cache_provider)):
    return cache.get(device_id)


@router.post('/api/device/{device_id}/config',
             response_model=DeviceUI)
def set_device_config(
        device_id: str,
        config: Config,
        cache=Depends(cache_provider)):
    device = cache.get(device_id)
    device.config = config
    device.synced = False
    cache.set(device_id, device)
    return device

