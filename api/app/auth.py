import jwt
from app.local_settings import RSA_PUBLIC_KEY
from fastapi import HTTPException

def jwt_decode(token, public_key=RSA_PUBLIC_KEY, audience="sqn-iot"):
    """
        Decodes JWT token. Only RSA encryption is currently supported
        Current implementation uses a single key pair for all devices
    """
    if token is None:
        # Device authentication is optional for now
        return

    parts = token.split(' ')

    if parts[0].strip() != "Bearer" or len(parts) is not 2:
        raise HTTPException(
            status_code=401,
            detail="Failed JWT authentication: \
                authorization header not well formed")

    token = parts[1].strip()

    try:
        jwt.decode(token, public_key, algorithms='RS256', audience=audience)
        logger.info(" Successfully Verified JWT token")
    except jwt.InvalidTokenError as e:
        raise HTTPException(
            status_code=401,
            detail="Failed JWT authentication: {}".format(e))


