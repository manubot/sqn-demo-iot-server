
from pymemcache.client.base import Client
from pymemcache import serde
from fastapi import HTTPException
import os


class Cache():

    def __init__(self, server, expire_days):
        self.memcache = Client(
            (server, 11211),
            serializer=serde.python_memcache_serializer,
            deserializer=serde.python_memcache_deserializer)
        self.expire_days = expire_days

    def get(self, device_id):
        self.device = self.memcache.get(device_id)
        if self.device:
            return self.device
        else:
            raise HTTPException(
                status_code=404,
                detail="device imei %s has not been found" % device_id)

    def set(self, key, value):
        self.memcache.set(key, value, expire=self.expire_days * 24 * 3600)


def cache_provider():
    # When in production assuming "memcached" is network name of memcache server
    production = 'production' in os.environ and os.environ['production'] == "yes"
    memcache_server = "memcached" if production else "localhost"
    EXPIRE_DELAY_DAYS = 7  # Amount of days data will be retained in memcache
    return Cache(memcache_server, EXPIRE_DELAY_DAYS)
