from fastapi import FastAPI

from starlette.middleware.cors import CORSMiddleware
from app.local_settings import ORIGINS_PRODUCTION_SERVER

from .routers import ui_api, device_api


app = FastAPI(
    title="Sequans Demo Server",
    description="API documentation for interfacing with HTTP REST API of Sequans demo server",
    version="0.2.0",)
app.include_router(device_api.router, tags=["Device API"])
app.include_router(ui_api.router, tags=["UI API"])

origins = [
    "http://localhost",
    "http://localhost:4200",
    ORIGINS_PRODUCTION_SERVER
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
