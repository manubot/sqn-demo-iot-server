
#### Local development setup

1. Requires python3.6+
1. Requires local memcached server. The easiest is to run one on docker: 
     - `docker run -p 11211:11211 --name local-memcache -d memcached`
     - Other helpful commands:
         + `docker stop local-memcache` if you want to stop the memcache service
         + `docker start local-memcache` if you want to restart the memcache service
         + `docker rm local-memcache` when you are done
1. Requires installing requirements:
    
    - `cd app`
    - `pip install -r local-requirements.txt`

    It is advised to run from virtual environment:
     - `virtualenv env`
     - `source env/bin/activate`   
     
    Alternatively use pyenv:
     - `pyenv virtualenv env_fastapi`
     - `pyenv activate env_fastapi`
     - `pyenv deactivate` when you are done

1. To run the server:

    `cd api`
    `uvicorn app.main:app --reload`

The API will then be available at `http://localhost:8000/``

##### API Documentation

API documentation is available at following endpoints
- `/docs`for Swagger docs format
- `/redoc` for redoc format
