import { Component, OnInit, OnDestroy } from '@angular/core';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { SharedModule} from '../shared/shared.module';

import { ActivatedRoute , Router} from '@angular/router';
import { Location } from '@angular/common';

import { Device } from '../components/device/device.model';
import { Config } from '../components/device/config.model';
import { DeviceService } from '../components/device/device.service';

import { MatSnackBar } from '@angular/material/snack-bar';

export enum EventType {
  ButtonPress = 'button_press',
  SwitchOn = 'switch_on',
  SwitchOff = 'switch_off'
}

export enum LowPowerMode {
  eDRX = 'eDRX',
  PSM = 'PSM',
  IDLE = 'IDLE'
}

@Component({
  selector: 'app-device-dashboard',
  templateUrl: './device-dashboard.component.html',
  styleUrls: ['./device-dashboard.component.scss']
})
export class DeviceDashboardComponent implements OnInit, OnDestroy {


  device: Device;
  sensorData = {}
  interval: any;
  year = new Date().getFullYear();

  get eventTypes(){
    return EventType;
  }
  get lowPowerMode(){
    return LowPowerMode;
  }

  /** Based on the screen size, switch layout **/
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return {
          position : { title : 'Device Position', cols : 2, rows : 2},
          sensor : { title : 'Sensor Data', cols : 2, rows: 1},
          events : { title : 'Events', cols : 1, rows: 1},
          control : { title : 'Control', cols : 1, rows: 1},
        };
      }

      return {
        position : { title : 'Device Position', cols : 1, rows : 2},
        sensor : { title : 'Sensor Data', cols : 1, rows: 1},
        events : { title : 'Events', cols : 1, rows: 1},
        control : { title : 'Control', cols : 1, rows: 1},
      };
    })
  );

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
  );

  ngOnInit(): void {
    this.getDevice();
    this.interval = setInterval(() => {
                this.getDevice();
    }, 5000);
  }

  ngOnDestroy(): void {
    clearInterval(this.interval);
  }

  sensorChartMaxLength = 20;

  checkSensorData(): void {
    if (this.device.sensors == null) return;

    this.device.sensors.forEach((sensor, i) => {

      if (sensor.sensor_type in this.sensorData) {
        let data = this.sensorData[sensor.sensor_type].data;
        const lastTime = data[data.length - 1].x;
        if (sensor.timestamp > lastTime) {
          // if (data.length >= this.sensorChartMaxLength) {
          //   data.shift();
          // }
          data.push({y: sensor.value, x: sensor.timestamp});
        }
        //workaroudn for https://stackoverflow.com/questions/60030400/dynamically-updating-time-data-in-chart-js-freezes-the-chart
        this.sensorData[sensor.sensor_type].data = data.slice(data.length > this.sensorChartMaxLength ? 1: 0);

      } else {
        this.sensorData[sensor.sensor_type] = {
          data : 
            [{
              x : sensor.timestamp,
              y : sensor.value
            }, ],
          units : sensor.units
        };
      }

    });

  }

  test_data = [{
    x: new Date(),
    y: 1
  },
  {
    x: new Date().setHours(new Date().getHours() +1),
    y: 2
  },
  {
    x: new Date().setHours(new Date().getHours() +2),
    y: 3
  }
  ]

  events = [];
  maxDisplayEvents = 5;


  checkEvents(): void {
    if (this.device.events == null) return;

    let new_events = []
    let lastEventTS = new Date('1/1/1999');
    if (this.events[this.events.length - 1]){
      lastEventTS = new Date(this.events[this.events.length - 1].timestamp);
    }
    this.device.events.forEach((event, i) => {
      if (event.timestamp > lastEventTS) {
        new_events.push(event);
      }
    });

    new_events.forEach((event, i) => {
      this.events.push(event);
      if (this.events.length > this.maxDisplayEvents){
        this.events.shift();
      }
    });

  }


  private process_device(device){
    this.device = device;
    this.checkSensorData();
    this.checkEvents();
  }

  setDeviceConfig(){
    this.deviceService.set_config(this.device.imei, this.device.config).subscribe (
      (device: Device) => {
      this.process_device(device);
      this._snackBar.open("Device Configuration Saved", "Saved", {
        duration: 3000,
      });
      }
    );
  }

  getDevice() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.deviceService.get(id).subscribe(
      (device: Device) => {
      this.process_device(device);
      },
      (error) => {
      this.router.navigate(['/']);
      }
    );
  }
  constructor(private breakpointObserver: BreakpointObserver,
              private deviceService: DeviceService,
              private route: ActivatedRoute,
              private location: Location,
              private router: Router,
              private _snackBar: MatSnackBar) {}

}
