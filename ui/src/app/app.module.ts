import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DeviceDashboardComponent } from './device-dashboard/device-dashboard.component';
import { SharedModule } from './shared/shared.module'
import { LoginComponent } from './login/login.component';
import { HttpErrorHandler } from './http-error-handler.service';
import { AgmCoreModule } from '@agm/core';

import { HttpClientModule } from '@angular/common/http';

import { MomentModule } from 'ngx-moment';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatCardModule } from '@angular/material/card';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { SensorChartComponent } from './components/sensor-chart/sensor-chart.component';

@NgModule({
  declarations: [
    AppComponent,
    DeviceDashboardComponent,
    LoginComponent,
    SensorChartComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDB6gKir6UzsdQlzVVD5vx8CMO_watuBR0'
    }),
    HttpClientModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    ReactiveFormsModule,
    FormsModule,
    ChartsModule,
    MomentModule
  ],
  providers: [HttpErrorHandler],
  bootstrap: [AppComponent]
})
export class AppModule { }
