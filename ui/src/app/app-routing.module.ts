import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeviceDashboardComponent } from './device-dashboard/device-dashboard.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [

  { path: 'device/:id', component: DeviceDashboardComponent},
  { path: '', component: LoginComponent},
  { path: '**', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 

}
