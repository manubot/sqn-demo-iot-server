import { Component } from '@angular/core';
import { FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  addressForm = this.fb.group({
    imei: ['', [this.ImeiValidator(), Validators.required]],
  });

  ImeiValidator(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      let val = this.removeDashes(control.value)
      let res = true;
      res = res && !isNaN(+val) 
      res = res && (val.length >= 15) && (val.length <= 17)

      return !res ? {'invalid IMEI': {value: control.value}} : null;
    };
  }

  removeDashes(value: string) {
    return value.replace(/-/g,'');
  }

  constructor(
      private fb: FormBuilder,
      private router: Router
      ) {}

  onSubmit() {

    this.router.navigate(['/device', this.removeDashes(this.addressForm.controls.imei.value)]);
  }
}
