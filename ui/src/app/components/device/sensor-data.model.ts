export class SensorData {
    timestamp: Date;
    value: number;
    sensor_type: string;
    units: string;
}
