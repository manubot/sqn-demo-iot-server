
import { CellIdObs } from './cellidobs.model';
import { Position } from './position.model';
import { SensorData } from './sensor-data.model';
import { Capabilties } from './capabilities.model';
import { Event } from './event.model';
import { Config } from './config.model';

export class Device {
  imei: number;
  chipset: string;
  hardware: string;
  last_apn: string;
  last_update: Date;
  created_at: Date;
  position: Position;
  cellid: CellIdObs;
  sensors: SensorData[];
  capabilities: Capabilties;
  events: Event[];
  config: Config;
  synced: boolean;

}