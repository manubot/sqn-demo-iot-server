import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Device } from './device.model';
import { Config } from './config.model';
import { HttpErrorHandler, HandleError } from '../../http-error-handler.service';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DeviceService {


  private handleError: HandleError;
  baseUrl = environment.api + 'device/';

  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('DeviceService');
  }
 
  private process_device(device: Device): Device {
    // Explicitely map dates to UTC
    device.created_at = new Date(device.created_at + 'Z');
    device.last_update = new Date(device.last_update + 'Z');
    if (device.sensors != null) {
      device.sensors.forEach((sensor, i) => sensor.timestamp = new Date(sensor.timestamp + 'Z'));
    }
    if (device.events != null) {
      device.events.forEach((event, i) => event.timestamp = new Date(event.timestamp + 'Z'));
    }
    if (device.config.leds == null) {
      device.config.leds = [false, false];
    }
    if (device.position){
      device.position.timestamp = new Date(device.position.timestamp + 'Z');
    }

    return device;
  }

  get(id: number) {
    const deviceUrl = this.baseUrl + id;
    return this.http.get<Device>(deviceUrl)
      .pipe(
        map(device => {
          return this.process_device(device);
        }),
        catchError(this.handleError('get'))
      );
  }

  set_config (id:number, config:Config){
    const deviceUrl = this.baseUrl + id + '/config';
    return this.http.post<Device>(deviceUrl, config)
      .pipe(
        map(device => {
          return this.process_device(device);
        }),
        catchError(this.handleError('set_config'))
      );
  }

}
