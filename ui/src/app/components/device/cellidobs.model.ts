export class CellIdObs{
    mcc : number;
    mnc : number;
    lac : number;
    cid : number;
    age : number;
    sstrength : number;
    tadvance : number;
}
