export class Position{
    accuracy: number;
    latitude: number;
    longitude: number;
    timestamp: Date;
}