import { Component, Input, OnInit, OnChanges, SimpleChange  } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label } from 'ng2-charts';

@Component({
  selector: 'app-sensor-chart',
  templateUrl: './sensor-chart.component.html',
  styleUrls: ['./sensor-chart.component.scss']
})
export class SensorChartComponent implements OnInit {

  @Input() labels: string [] ;
  @Input() legend: string;
  @Input() units: string;
  @Input() maxLength: number = 10;
  @Input() data: number[];

  public lineChartData: ChartDataSets[] = [
      { data: [], label: "" },
      ];
  public lineChartLabels: Label[];
  public lineChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
  };
  public lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,0,0,0.3)',

    },
  ];
  public lineChartLegend = false;
  public lineChartType: ChartType = 'line';
  public lineChartPlugins = [];
  public lineChartMaxLength = 10;

  //Workaround for https://stackoverflow.com/questions/60030400/dynamically-updating-time-data-in-chart-js-freezes-the-chart
  ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
      for (let propName in changes) {
        let changedProp = changes[propName];
        if (propName == 'data'){
          this.lineChartData[0].data = changedProp.currentValue.slice()
        }
      }  
    }

  ngOnInit() {
    this.lineChartData = [
      { data: this.data, label: this.legend }
    ];
    this.lineChartLabels = this.labels;
    this.lineChartOptions.scales = {
      yAxes :
      [
        {
          scaleLabel: {
            display: true,
            labelString: this.units
          }
        }
      ],
      xAxes:
      [
        {
          type: 'time',
          ticks:{
            source: 'auto'
          },
          time: {
            unit: 'second',
            displayFormats: {
              second: 'H:mm:ss'
          }
          },
          
        }
      ]
    };
  }

  constructor() { }


}
