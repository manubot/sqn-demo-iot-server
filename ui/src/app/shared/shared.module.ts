import { NgModule } from '@angular/core';


import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { LayoutModule } from '@angular/cdk/layout';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatRadioModule } from '@angular/material/radio';
import { MatSliderModule } from '@angular/material/slider'

@NgModule({
  
  imports: [MatCheckboxModule,
   MatGridListModule,
   MatCardModule,
   MatMenuModule,
   MatIconModule,
   MatButtonModule,
   LayoutModule,
   MatSidenavModule,
   MatListModule,
   MatToolbarModule,
   MatSnackBarModule,
   MatTooltipModule,
   MatExpansionModule,
   MatSlideToggleModule,
   MatRadioModule,
   MatSliderModule
   ],


  exports: [MatCheckboxModule,
   MatGridListModule,
   MatCardModule,
   MatMenuModule,
   MatIconModule,
   MatButtonModule,
   LayoutModule,
   MatSidenavModule,
   MatListModule,
   MatToolbarModule,
   MatSnackBarModule,
   MatTooltipModule,
   MatExpansionModule,
   MatSlideToggleModule,
   MatRadioModule,
   MatSliderModule
   ],

})
export class SharedModule { }
